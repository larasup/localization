<?php

namespace Larasup\Localization\Contracts;

interface iLocalize
{
    public function getLocalization($key, null|string $language = null): mixed;

    public function getLocalizationBulk(string|null $languageCondition = null): array;

    public function setLocalization(string $key, string $value, null|string $language = null): void;

    public function setLocalizationBulk(array $data, null|string $language = null): void;

    public function localization();

    public function getMorphName(): string;
}