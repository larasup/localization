<?php

namespace Larasup\Localization\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property $id
 * @property $object_class
 * @property $model_primary_value
 * @property $language_code
 * @property $key
 * @property $value
 */
class Localization extends Model
{
    protected $table = 'localizations';

    protected $fillable = ['object_class', 'model_primary_value', 'language_code', 'key', 'value'];

    public $timestamps = false;
}
