<?php

namespace Larasup\Localization\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $code
 * @property string $name
 * @property string $emoji_flag
 * @property $status
 */
class Language extends Model
{
    protected $table = 'languages';

    public $incrementing = false;

    protected $primaryKey = 'code';

    protected $fillable = ['code', 'name', 'status'];
}
