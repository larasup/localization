<?php

namespace Larasup\Localization\Services;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Larasup\Localization\Enums\Statuses;

class LanguageService
{
    private const LANGUAGE_CACHE_KEY = 'languages';

    public static function getLanguageList(): Collection
    {
        return Cache::remember(self::LANGUAGE_CACHE_KEY, 60 * 60 * 24 * 7, function () {
            return Language::query()
                ->where('status', Statuses::Enabled->value)
                ->orderBy('code')
                ->get();
        });
    }
}