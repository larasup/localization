<?php

namespace Larasup\Localization\Services;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Larasup\Localization\Contracts\iLocalize;
use Larasup\Localization\Models\Localization;

class LocalizationService
{
    public static function setLocalization(string $modelName, string $primaryKey, string $field, string $value, null|string $language = null, $isNeedCacheUpdate = true): void
    {
        /** @var Model $modelNamespace */
        $modelNamespace = Relation::getMorphedModel($modelName) ?? $modelName;

        $model = $modelNamespace::query()
            ->find($primaryKey);

        if ($model instanceof iLocalize) {
            $model->setLocalization($field, $value, $language, $isNeedCacheUpdate);
        }
    }

    public static function enrichCollection(Collection $items, string $objectClass): void
    {
        $instance = new $objectClass;
        $primaryKey = $instance->getKeyName();

        $primaryList = collect();
        self::getIdsWithNested($items, $primaryList, $primaryKey);

        $language = App::getLocale();

        $localizations = self::getLocalization($instance::class, $primaryList, $language);
        $localizeFields = $instance::getLocalizeFields();

        self::localizeNestedFields($items, $language, $localizations, $localizeFields, $primaryKey);
    }

    private static function getIdsWithNested(Collection $items, Collection &$ids, string $primaryKey = 'id'): void
    {
        $items->each(function ($item) use (&$ids, $primaryKey) {
            $ids->push($item->$primaryKey);
            if ($item->children->isNotEmpty()) {
                self::getIdsWithNested($item->children, $ids, $primaryKey);
            }
        });
    }

    public static function getLocalization(
        string                          $objectClass,
        Collection|LengthAwarePaginator $collection,
        string|null                     $language = null
    ): Collection {
        /** @var Model $modelObject */
        $modelObject = new $objectClass;
        $morphName = $modelObject->getMorphClass();

        if ($collection instanceof LengthAwarePaginator) {
            $collection = $collection->getCollection()
                ->pluck($modelObject->getKeyName());
        }

        return self::getLocalizationByIdentifierList($morphName, $collection, $language);
    }

    private static function getLocalizationByIdentifierList(
        string      $morphName,
        Collection  $idList,
        string|null $language = null
    ): Collection {
        return Localization::query()
            ->where('object_class', $morphName)
            ->whereIn('model_primary_value', $idList)
            ->when($language, fn($query) => $query->where('language_code', $language))
            ->get()
            ->groupBy('model_primary_value')
            ->map(function ($item) {
                return $item->groupBy('language_code')
                    ->map(fn($item) => $item->pluck('value', 'key'));
            });
    }

    private static function localizeNestedFields(
        Collection $items,
        string     $language,
        Collection $localizations,
        array      $localizeFields,
        string     $primaryKey = 'id',
    ): void {
        $items->each(function ($item) use ($language, $localizations, $localizeFields, $primaryKey) {
            foreach ($localizeFields as $field) {
                $item->$field = $localizations[$item->$primaryKey][$language][$field] ?? null;
            }
            if ($item->children->isNotEmpty()) {
                self::localizeNestedFields(
                    $item->children,
                    $language,
                    $localizations,
                    $localizeFields,
                    $primaryKey
                );
            }
        });
    }
}