<?php

namespace Larasup\Localization\Enums;

enum Statuses: int
{
    case Disabled = 0;

    case Enabled = 1;
}
