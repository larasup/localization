<?php

namespace Larasup\Localization\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Larasup\Localization\Models\Localization;
use Exception;

/**
 * @property string $primaryKey
 * @property array $localize
 */
trait Localize
{
    public array $localizeData = [];

    public static function getLocalizeFields(): array
    {
        return (new self)->localize;
    }

    /**
     * @throws Exception
     */
    public function setLocalizationBulk(array $data, null|string $language = null): void
    {
        foreach ($data as $key => $value) {
            $this->setLocalization($key, $value, $language, false);
        }

        $this->updateCache($language);
    }

    /**
     * @throws Exception
     */
    public function setLocalization(string $key, string $value, null|string $language = null, $isNeedCacheUpdate = true): void
    {
        $this->validateLocalizationBeforeWork($key);

        $objectName = $this->getMorphName();
        $primaryKey = $this->primaryKey;
        $systemLanguage = App::getLocale();

        Localization::query()
            ->updateOrCreate(
                [
                    'object_class' => $objectName,
                    'model_primary_value' => $this->$primaryKey,
                    'language_code' => $language ?? $systemLanguage,
                    'key' => $key
                ],
                [
                    'value' => $value
                ]
            );

        if (is_null($language) || $language == $systemLanguage) {
            $this->localizeData[$key] = $value;
        }

        if ($isNeedCacheUpdate) {
            $this->updateCache($language);
        }
    }

    /**
     * @throws Exception
     */
    private function validateLocalizationBeforeWork($key): void
    {
        if (isset($this->localize) && !in_array($key, $this->localize)) {
            throw new Exception("Field $key is not in localize array");
        }

        $primaryKeyName = $this->primaryKey;
        if (is_null($this->$primaryKeyName)) {
            throw new Exception("Model doesn't have primary key");
        }
    }

    public function getMorphName(): string
    {
        /** @var $this Model */
        try {
            return $this->getMorphClass();
        } catch (Exception) {
            return get_class($this);
        }
    }

    private function updateCache(string|null $language = null): void
    {
        $cacheKey = $this->getCacheKey($language);
        Cache::put($cacheKey, $this->localizeData, 60 * 60 * 24 * 7);
    }

    private function getCacheKey(string|null $language = null): string
    {
        $language = $language ?? Config::get('app.fallback_locale');
        $primaryKey = $this->primaryKey;
        return 'localization_' . $this->getMorphName() . '_' . $this->$primaryKey . '_' . $language;
    }

    /**
     * @throws Exception
     */
    public function __get($key): mixed
    {
        return in_array($key, $this->localize)
            ? $this->getLocalization($key)
            : parent::__get($key);
    }

    /**
     * @throws Exception
     */

    public function getLocalization($key, null|string $language = null): mixed
    {
        $this->validateLocalizationBeforeWork($key);
        $appLocale = App::getLocale();

        if (
            empty($this->localizeData)
            || (!is_null($language) && $language != $appLocale)
        ) {
            $this->getLocalizationBulk($language ?? $appLocale);
        }

        if (Config::get('app.debug') && !isset($this->localizeData[$key])) {
            $primaryKeyName = $this->primaryKey;
            $errorMessage = 'Localization not found for field "' . $key .
                '". (Additional data: Model primary value: "' . $this->$primaryKeyName .
                '", Model: "' . $this->getMorphName() . '")';
            throw new Exception($errorMessage);
        }

        return $this->localizeData[$key] ?? null;
    }

    public function getLocalizationBulk(string|null $language = null): array
    {
        $cacheKey = $this->getCacheKey($language);

        return $this->localizeData = Cache::remember($cacheKey, 60 * 60 * 24 * 7, function () use ($language) {
            $builder = $this->localization()
                ->select('key', 'value', 'language_code');

            if (!is_null($language)) {
                if ($language != Config::get('app.fallback_locale')) {
                    $languageCondition = [$language, Config::get('app.fallback_locale')];
                    $builder->whereIn('language_code', $languageCondition);
                } else {
                    $builder->where('language_code', $language);
                }
            }

            $localization = $builder->get()
                ->groupBy('language_code')
                ->map(fn($item) => $item->pluck('value', 'key')->toArray())
                ->toArray();

            foreach ($localization['en'] ?? [] as $key => $value) {
                $this->localizeData[$key] = $localization[$language][$key] ?? $value;
            }

            return $this->localizeData;
        });
    }

    public function localization(): HasMany
    {
        /** @var $this Model */
        return $this->hasMany(Localization::class, 'model_primary_value', $this->primaryKey)
            ->where('object_class', $this->getMorphName());
    }
}
